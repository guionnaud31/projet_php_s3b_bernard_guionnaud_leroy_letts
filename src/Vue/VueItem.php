<?php

namespace wishlist\Vue;
use wishlist\modele\Item as Item;
use wishlist\modele\Item as ModeleItem;
class VueItem extends Vue{

    function __construct() {
        parent::__construct();
    }

    public function renderItems(){
        $res="";
        $urlajoutitem = \Slim\Slim::getInstance()->urlFor('additem');
        $listes = ModeleItem::all();
        foreach ($listes as $liste) {
            $i =1;
            $urlitems = \Slim\Slim::getInstance()->urlFor("/item",["id"=> $liste->id]);
            $res = $res.($liste->id).'. '.("<a href='$urlitems' title=\"aller à la liste\">$liste->nom</a>". "<br>");
            $i++;
        }

        $this->content .= "<center>Affichage de tous les items<br></center></br>" . $res. 
        "<br><a href=\"$urlajoutitem\" title=\"aller à la section creer item\">Ajouter item</a>";
        $this->render();
    }

    public function renderItemListe($items)
    {
        $urlajoutitem = \Slim\Slim::getInstance()->urlFor('additem');
        $urlpartage = \Slim\Slim::getInstance()->urlFor('partage');
        $urlsuplis = \Slim\Slim::getInstance()->urlFor("suplis",["id"=> $items[0]->liste_id]);
        $res="";
        $listes = $items;
        foreach ($listes as $liste) {
            $i =1;
            $urlliste = \Slim\Slim::getInstance()->urlFor("/item",["id"=> $liste->id]);
            $res = $res.($liste->id).'. '.("<a href='$urlliste' title=\"voir l'item\">$liste->nom</a>". "<br>");
            $i++;
        }
        $res .= "<center><form action=\"$urlpartage\" method=\"POST\">
 
        <input type=\"submit\" value=\"Partager la liste\">
      </form>";
        if (isset($items[0])) {
            # code...
            $this->content .= "<center>Affichage de tous les items de la liste ". $items[0]->liste_id ."<br></center>" . $res . 
        "<br><a href=\"$urlajoutitem\" title=\"aller à la section creer item\">Ajouter item</a>".
        "<br><a href=\"$urlsuplis\" title=\"Supprime la liste actuelle\">Supprimer Liste</a>";
        }
        else {
            # code...
            $this->content .= "<center>Affichage de tous les items de la liste "."<br></center>" . $res . 
        "<br><a href=\"$urlajoutitem\" title=\"aller à la section creer item\">Ajouter item</a>";
        }
        //$this->content .= "<center>Affichage de tous les items de la liste ". $items[0]->liste_id ."<br></center>" . $res . 
        //"<br><a href=\"$urlajoutitem\" title=\"aller à la section creer item\">Ajouter item</a>";

        $this->render();
    }
    public function renderItemUnique($item)
    {
        $res="";
        $listes = $item;
        $urlsupitem = \Slim\Slim::getInstance()->urlFor("supitem",["id"=> $item->id]);
        $this->content .= "<center>Affichage de l'item <br></center>" . $listes->id.". "."<a href='$listes->url' title=\"voir l'item\">".$listes->nom."</a> ". "dans la liste ". $listes->liste_id. "     ".  "<img src='../img/$listes->img'>".
        "</br></br> <br><a href=\"$urlsupitem\" title=\"Supprime l'item'\">Supprimer Item</a>";
        $this->render();
    }

    function renderForm(){   
        $urladditem = \Slim\Slim::getInstance()->urlFor('ajoutItem');

        $this->content = "
        <form action=\"$urladditem\" method=\"POST\">

        <p>Nom : <input type=\"text\" name=\"Nom\" /></p>
        <p>Description : <input type=\"text\" name=\"ItemDescription\" /></p>
        <p>Prix : <input type=\"text\" name=\"Prix\" /></p>
        <p>User ID : <input type=\"text\" name=\"UsrId\" /></p>
        <p>Liste ID : <input type=\"text\" name=\"ListeId\" /></p>
        <p>Image : <input type=\"text\" name=\"Img\" /></p>
        <p>URL item : <input type=\"text\" name=\"Link\" /></p>

        <p><input type=\"submit\" value=\"OK\"></p>
        </form>
        ";
        $this->render();
    }

    function renderAjout(){   
        $this->content = "<p>Votre item à bien été ajouté</p>";
        $this->render();
    }
    public function partageLien()
    {
        # code...
        $this->content="<center>Veuillez copier le lien: ". $_SERVER["HTTP_REFERER"]."</center>";
        $this->render();
    }

    function renderSupr(){   
        $this->content = "<p>L'item à bien été supprimé</p>";
        
        $this->render();
    }
}