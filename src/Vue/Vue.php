<?php

namespace wishlist\Vue;
use wishlist\modele\Liste as Liste;
use wishlist\modele\Item as ModeleItem;
class Vue{
    
    protected $listes;
    protected $content;
    public function __construct(){
        $this->listes = Liste::all();
        $this->content="";
        
    }


    public function render(){
        $titre = $this->RenderTitle();
        $menu = $this->RenderMenu();
        $foot = $this->RenderFooter();
        if($this->content==""){
            $this->content=$this->Renderaccueil();
        }
        $html = <<<END
<!DOCTYPE html>
<html>
<head>
<title> $titre  </title>
<meta charset='utf-8'>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="../css/bootstrap.css" rel="stylesheet" media="all">
<link href="css/bootstrap.css" rel="stylesheet" media="all">

</head>

<body>
<h1>
<center>  $titre  </center>
</h1>
<div class="menu">
$menu
</div>
<div class="content">
$this->content
</div>
</body>
<footer>
$foot
</footer>
</html>
END;
        echo $html;

    }
    
    public function RenderTitle(){
        return "MyWishlist";
    }

    public function RenderMenu(){
        $urlaccueil = \Slim\Slim::getInstance()->urlFor('accueil');
        $urlCreateList = \Slim\Slim::getInstance()->urlFor('list_creer');
        $urllist = \Slim\Slim::getInstance()->urlFor('list_getLists');
        $urlitem = \Slim\Slim::getInstance()->urlFor('item_getAllItems');

        return <<<END
        <ul id="navigation">
        <li><a href='$urlaccueil' title="aller à l'accueil">Acceuil</a></li>
        <li><a href="$urllist" title="aller au menu des listes">Listes</a></li>
        <li><a href="$urlitem" title="aller au menu des items">Items</a></li>
        <li><a href="$urlCreateList" title="créer une liste">Créer Liste</a></li>
      </ul>
END;
    }

    public function RenderFooter(){
        return "<p> Conditions générales d'utilisation </p>
        <p> Mentions légales </p>
        <p> © Copyright 2020 </p>
        <p><a href='https://www.google.com/search?rlz=1C1CHBD_frFR875FR875&sxsrf=ACYBGNS9UFKf7A8_YGJZBN5O3fvGVkGeTg%3A1579107661393&ei=TUUfXrHPF5ScjLsPheW4sAw&q=vider+base+de+donn%C3%A9es+phpmyadmin&oq=r%C3%A9initialiser+base+de+donn%C3%A9es+php&gs_l=psy-ab.3.0.0i22i30l2.5268.6466..7778...0.2..0.77.264.4......0....1..gws-wiz.......0i71.zWYrMvFZd4U'> Reset database </a></p>";
    }


    public function Renderaccueil(){
        return <<<END
        <div id=\"accueil\">
        <p>Vous souhaitez créer une liste de souhaits pour un anniversaire ou un mariage ? Ou tout autre événement vous tenant à coeur ? 
        </br></br>
        Vous vous trouvez au bon endroit !</p>
        </br></br>
        <p>Les listes actuelles sont disponibles en cliquant sur l'onglet Listes. Vous pouvez avoir accès aux listes publiques, mais également aux listes privées auxquelles vous auriez été invités.</p>
        </br>
        <p>Vous pouvez bien évidemment créer vos propres listes et les partager.</p>
        </br>
        <p>La fête n'attend plus que vous !</p>
        </div>
END;
    }

}