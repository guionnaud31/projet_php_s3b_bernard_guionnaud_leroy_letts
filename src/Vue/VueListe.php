<?php

namespace wishlist\Vue;
use wishlist\modele\Liste as Liste;
use wishlist\modele\Item as ModeleItem;
class VueListe extends Vue{
    
    function __construct() {
        parent::__construct();
    }

    public function renderListes(){
        $res="";
        $listes = Liste::all();
        foreach ($listes as $liste) {
            $urlliste = \Slim\Slim::getInstance()->urlFor("/listeitem", ["id" => $liste->no]);
            $res = $res.($liste->no).'. '.("<a href='$urlliste' title=\"aller à la liste\">$liste->titre</a>". "<br>");
        }
        $this->content .= "<center>Affichage de toutes les listes<br></center></br>" .$res;
        $this->render();
    }

    function renderForm(){   
        //$urlcreer = \Slim\Slim::getInstance()->urlFor('accueil');
        $urlCreateList = \Slim\Slim::getInstance()->urlFor('creer_list');

        $this->content = "
        <form action=\"$urlCreateList\" method=\"POST\">

        <p>Titre : <input type=\"text\" name=\"Titre\" /></p>
        <p>Description : <input type=\"text\" name=\"Description\" /></p>
        <p>Date d'expiration : <input type=\"date\" name=\"Date\" /></p>
        <p>User ID : <input type=\"text\" name=\"UsrId\" /></p>
        <p><input type=\"submit\" value=\"OK\"></p>
        </form>
        ";
        $this->render();
    }

    function renderCreer(){   
        $this->content = "<p>Votre WishList à bien été ajoutée</p>";
        
        $this->render();
    }



    function renderSupr(){   
        $this->content = "<p>La WishList à bien été supprimée</p>";
        
        $this->render();
    }
}