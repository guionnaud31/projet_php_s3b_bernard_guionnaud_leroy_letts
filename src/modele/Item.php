<?php

namespace wishlist\modele;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends \Illuminate\Database\Eloquent\Model {
    //use SoftDeletes;
    protected $table = 'item';
    protected $primaryKey = 'id';
    public $timestamps = false;
    //protected $dates = ['deleted_at'];

    /**
     * Permet de regrouper les items par liste
     */
    public function liste() {
      return $this->belongsTo('mywishlist\modele\Liste','liste_id','id');
    }

}