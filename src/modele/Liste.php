<?php

namespace wishlist\modele;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\QueryException;

class Liste extends \Illuminate\Database\Eloquent\Model {
    //use SoftDeletes;
    protected $table = 'liste';
    protected $primaryKey = 'no';
    public $timestamps = false;
    //protected $dates = ['deleted_at'];

    /**
     * Compte le nombre d'item d'une liste
     */
    public function items() {
        return $this->hasMany('mywishlist\modele\Item','liste_id','no');
      }
}