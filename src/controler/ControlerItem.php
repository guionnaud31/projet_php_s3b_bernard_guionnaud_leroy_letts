<?php
namespace wishlist\controler;
require_once 'vendor/autoload.php';

use wishlist\modele\Item as ModeleItem;
use wishlist\modele\Liste as Liste;
use wishlist\Vue\Vue as Vue;
use wishlist\Vue\VueItem as VueItem;

class ControlerItem{

    public function __construct(){
    }

    public function affichageItem()
    {
        $v = new VueItem();
        $v->renderItems();
    }

    public function ajoutItemForm()
    {
        $v = new VueItem();
        $v->renderForm();
    }
    
    
    public function affichageItemID($idd)
    {
        # code...
        $item = ModeleItem::where(['id' => $idd])->first();
        $v = new VueItem();
        $v->renderItemUnique($item);
               // var_dump($item);

    }

    public function affichageItemListe($idListe)
    {
        # code...
        $items = ModeleItem::where(['liste_id' => $idListe])->get();
        $v = new VueItem();
        $v->renderItemListe($items);
    }

    public function affAddItem()
    {
        # code...
        $v = new VueItem();
        $v->renderForm();

    }

    public function ajoutItem()
    {
        $v = new VueItem();
        $item = new ModeleItem();
        //$liste = Liste::where(['liste_id' => $idListe])->get();
        //$liste->user_id = $_POST['UsrId'];
        $item->nom = filter_var($_POST['Nom'], FILTER_SANITIZE_STRING);
        $item->descr = filter_var($_POST['ItemDescription'], FILTER_SANITIZE_STRING);
        $item->tarif = filter_var($_POST['Prix'], FILTER_SANITIZE_STRING);
        $item->liste_id = filter_var($_POST['ListeId'], FILTER_SANITIZE_STRING);
        $item->img = filter_var($_POST['Img'], FILTER_SANITIZE_STRING);
        $item->url = filter_var($_POST['Link'], FILTER_SANITIZE_STRING);

        $item->save();
        $v->renderAjout();

    }
    public function partageListe()
    {
        # code...
        $v = new VueItem();
        $v->partageLien();
    }

    public function supprimerItem($id)
    {
        # code...
        $v = new VueItem();
        $item = ModeleItem::where(['id' => $id])->first();
        $item->delete();
        $v->renderSupr();
    }
}