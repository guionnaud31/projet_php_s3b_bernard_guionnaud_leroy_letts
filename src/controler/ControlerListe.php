<?php
namespace wishlist\controler;
require_once 'vendor/autoload.php';

use wishlist\modele\Item as ModeleItem;
use wishlist\modele\Liste as Liste;
use wishlist\Vue\Vue;
use wishlist\Vue\VueListe;
use wishlist\Vue\VueItem;
class ControlerListe{
    public function __construct(){
    }
    
    public function affichageDeListe(){
        $v = new VueListe();
        $v->renderListes();
    }

    public function affCreaListe()
    {
        # code...
        $v = new VueListe();
        $v->renderForm();

    }

    public function creerListe()
    {
        # code...
        $v = new VueListe();
        $liste = new Liste();
        $liste->user_id = $_POST['UsrId'];
        $liste->titre = filter_var($_POST['Titre'], FILTER_SANITIZE_STRING);
        $liste->description = filter_var($_POST['Description'], FILTER_SANITIZE_STRING);
        $liste->expiration = $_POST['Date'];
        $liste->token= rand();
        $liste->save();
        $v->renderCreer();

    }

    public function supprimerListe($id)
    {
        # code...
        $v = new VueListe();
        $liste = Liste::where(['no' => $id])->first();
        $liste->delete();
        $v->renderSupr();
    }
}