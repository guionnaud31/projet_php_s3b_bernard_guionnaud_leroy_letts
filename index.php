<?php
require_once  __DIR__. '/vendor/autoload.php' ;
use wishlist\controler\WishControler;
use wishlist\controler\ControlerListe;
use wishlist\controler\ControlerItem;
use wishlist\Vue\Vue;

use Illuminate\Database\Capsule\Manager as DB;
$db = new DB();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();
session_start();


$app = new \Slim\Slim ;

$app->get('/', function () {
    $ctrl = new WishControler();
    $ctrl->accueil();
})->name('accueil');


$app->get('/liste', function () {
    $ctrl = new ControlerListe();
    $ctrl->affichageDeListe();
})->name('list_getLists');

//$app->get('/listedeliste', function () {
  //  echo ("Affichage liste");
//});
$app->get('/listeitem', function () {
    $ctrl = new ControlerItem();
    $ctrl->affichageItem();
    //echo ("Affichage liste item");
})->name('item_getAllItems');

$app->get('/listeitem/:id', function ($id) {
    $cl = new ControlerItem();
    $cl->affichageItemID($id);
    //echo ("Affichage item " + 12);
})->name('/item');

$app->get('/itemliste/:id', function ($id) {
    $cl = new ControlerItem();
    $cl->affichageItemListe($id);
})->name('/listeitem');

$app->get('/creationListe', function(){
    $c=new ControlerListe();
    $c->affCreaListe();

})->name('list_creer');

$app->post('/creerListe', function(){
    $c=new ControlerListe();
    $c->creerListe();

})->name('creer_list');

$app->get('/suplist/:id', function ($id) {
    $cl = new ControlerListe();
    $cl->supprimerListe($id);
})->name('suplis');

$app->get('/itemAjout', function(){
    $c=new ControlerItem();
    $c->affAddItem();

})->name('additem');

$app->post('/ajoutItem', function(){
    $c=new ControlerItem();
    $c->ajoutItem();
    
})->name('ajoutItem');

$app->get('/supItem/:id', function ($id) {
    $cl = new ControlerItem();
    $cl->supprimerItem($id);
})->name('supitem');

$app->post('/partage', function(){
    $c=new ControlerItem();
    $c->partageListe();
})->name('partage');

$app->run();

