BERNARD Romain
GUIONNAUD Quentin
LEROY Théo
LETTS Tristan

Lien vers le site :

https://webetu.iutnc.univ-lorraine.fr/~bernar284u/projet_php_s3b_bernard_guionnaud_leroy_letts/

Fonctionnalités réalisées:

1 - Afficher une liste de souhaits (GUIONNAUD Quentin, LETTS Tristan)

2 - Afficher un item d'une liste (LETTS Tristan, GUIONNAUD Quentin)

6 - Créer une liste (BERNARD Romain, LETTS Tristan, GUIONNAUD Quentin)

8 - Ajouter des items (BERNARD Romain, GUIONNAUD Quentin, LETTS Tristan)

10 - Supprimer un item (GUIONNAUD Quentin, LETTS Tristan)

11 - Rajouter une image à un item (LETTS Tristan)

14 - Partager une liste (LETTS Tristan)

Possibilité de supprimer une liste manuellement, si on le souhaite  (GUIONNAUD Quentin, LETTS Tristan)